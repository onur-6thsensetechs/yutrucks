//
//  TrucksMapVC.swift
//  YuTrucks
//
//  Created by Onur YILDIRIM on 3/12/19.
//  Copyright © 2019 Yupanatech. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import MapKit


class TrucksMapVC: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate {
    
    let LINXUP_URL = "https://www.linxup.com/ibis/rest/api/v2/locations"
    let params : [String : String] = ["Content-Type" : "application/json", "Accept" : "application/json"]
    let headers : [String : String] = ["Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJjb21wYW55SWQiOiIyMzAyMDMiLCJpc3MiOiJhZ2lsaXMiLCJwZXJzb25JZCI6Ijg1NzExNyIsImV4cCI6MTcwODA0MDEwNCwiaWF0IjoxNTUwMjczNzA0LCJ1c2VybmFtZSI6Im9udXIueWlsZGlyaW1AeXVwYW5hdGVjaC5jb20ifQ.ROxOHsnWldaP08yqxlpYkq5h7AVHWpMiX2IoG8M9KoE"]
    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 5000
    var yupanaTrucks = [Truck]()
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.delegate = self
        mapView.register(TruckView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        //let yupanaSF = MKAnnotation()
        //yupanaSF.title = "Yupana Office"
        //yupanaSF.coordinate = CLLocationCoordinate2D(latitude: 38.0165, longitude: -122.0321)
        //xxxxxxxxxxxxxxmapView.addAnnotation(yupanaSF)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Location found :)")
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            let myLocation = CLLocation(latitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude))
            centerMapOnLocation(location: myLocation)
            getTruckLocations(url:LINXUP_URL, parameters: params, headers: headers)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
        print("Can not get location!")
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func getTruckLocations(url: String, parameters: [String: String], headers: [String: String]) {
        Alamofire.request(url, method: .post, parameters: parameters, headers:headers).responseJSON {
            response in
            if response.result.isSuccess {
                print("We got location data")
                let locationJSON : JSON = JSON(response.result.value!)
                print(locationJSON)
                self.updateLocationData(json: locationJSON)
            } else {
                print("There is an error \(String(describing: response.result.error))")
            }
        }
    }
    
    func updateLocationData(json : JSON){
        yupanaTrucks.removeAll()
        for i in 0...json["data"]["locations"].count - 1 {
            yupanaTrucks.append(Truck(no : i,
                                      title : json["data"]["locations"][i]["firstName"].string!,
                                      subtitle : json["data"]["locations"][i]["lastName"].string!,
                                      coordinate : CLLocationCoordinate2D(latitude: json["data"]["locations"][i]["latitude"].double!,
                                      longitude : json["data"]["locations"][i]["longitude"].double!),
                                      heading : json["data"]["locations"][i]["heading"].string!,
                                      speed : json["data"]["locations"][i]["speed"].int!,
                                      direction : json["data"]["locations"][i]["direction"].int!))
        }
        mapView.addAnnotations(yupanaTrucks)
    }

    @IBAction func closeBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


