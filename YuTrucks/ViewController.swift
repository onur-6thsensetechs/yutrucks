//
//  ViewController.swift
//  YuTrucks
//
//  Created by Onur YILDIRIM on 2/18/19.
//  Copyright © 2019 Yupanatech. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class ViewController: UIViewController, GIDSignInUIDelegate {


    
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        //GIDSignIn.sharedInstance().signIn()
        
        // TODO(developer) Configure the sign-in button look/feel
        // ...
    }

    override func viewDidAppear(_ animated: Bool) {
        if userDefault.bool(forKey: "userSignedIn") {
            performSegue(withIdentifier: "Segue_To_SignIn", sender: self)
        } else {
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Create User section.... But we dont create user in this app..
    //func createUser(email:String, password:String) {
    //    Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
    //        if error == nil {
    //            print("user has been created")
    //            self.signInUser(email: email, password: password)
    //        } else {
    //            print(error?.localizedDescription as Any)
    //        }
    //    }
    //}
    
    func signInUser(email:String, password:String){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error == nil {
                print("user signed in")
                self.userDefault.set(true, forKey: "userSignedIn")
                self.userDefault.synchronize()
                self.performSegue(withIdentifier: "Segue_To_SignIn", sender: self)
            } else {
                print(error as Any)
                print(error?.localizedDescription as Any)
                let alert = UIAlertController(title: "Error!", message: error!.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
}

