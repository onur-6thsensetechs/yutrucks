//
//  TruckViews.swift
//  YuTrucks
//
//  Created by Onur YILDIRIM on 3/28/19.
//  Copyright © 2019 Yupanatech. All rights reserved.
//

import Foundation
import MapKit

class TruckView: MKAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let truck = newValue as? Truck else {return}
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControl.State())
            rightCalloutAccessoryView = mapsButton
            
            if let imageName = truck.imageName {
                let pinImage = UIImage(named: imageName)
                let size = CGSize(width: 30, height: 20)
                UIGraphicsBeginImageContext(size)
                pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                image = resizedImage
            } else {
                image = nil
            }
        }
    }
}
