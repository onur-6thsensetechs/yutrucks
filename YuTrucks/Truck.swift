//
//  Truck.swift
//  YuTrucks
//
//  Created by Onur YILDIRIM on 3/6/19.
//  Copyright © 2019 Yupanatech. All rights reserved.
//

import Foundation
import MapKit

class Truck: NSObject, MKAnnotation {
    var no : Int
    var title : String?
    var subtitle : String?
    var coordinate : CLLocationCoordinate2D
    var heading : String
    var speed : Int
    var direction : Int
    
    init (no: Int, title: String?, subtitle : String?, coordinate: CLLocationCoordinate2D, heading : String, speed : Int, direction: Int) {
        self.no = no
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.heading = heading
        self.speed = speed
        self.direction = direction
        
        super.init()
    }
    
    var imageName: String? {
        return "truck"
    }
    
}
