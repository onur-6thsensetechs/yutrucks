//
//  SignedInVC.swift
//  YuTrucks
//
//  Created by Onur YILDIRIM on 2/22/19.
//  Copyright © 2019 Yupanatech. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class SignedInVC: UIViewController {

    @IBOutlet weak var lblEmail: UILabel!
    
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let email = Auth.auth().currentUser?.email else { return }
        lblEmail.text = email
    }
    

    @IBAction func btnSignOutPressed(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
            GIDSignIn.sharedInstance()?.signOut()
            userDefault.removeObject(forKey: "userSignedIn")
            userDefault.synchronize()
            self.dismiss(animated: true, completion: nil)
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }

}
